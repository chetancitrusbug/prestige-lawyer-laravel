<?php

return [

    'roles' => [
            '0' => [
                'label' => ' Admin',
                'name' => 'AU'
            ],
            '1' => [
                'label' => 'Resercher',
                'name' => 'RU'
            ],
            '2' => [
                'label' => 'Manager',
                'name' => 'MU'
            ],
			'3' => [
                'label' => 'Lawyer',
                'name' => 'LU'
            ],
        ],
		
	'users' => [
            '0' => [
                'name' => ' Admin User',
                'first_name' => 'Admin',
                'last_name' => '',
				'status'=>'active',
				'email'=>'admin@gmail.com',
				'password'=>'123456',
				'role'=>'AU',
            ],
            '1' => [
                'name' => 'Resercher User',
                'first_name' => 'Resercher',
                'last_name' => '',
				'status'=>'active',
				'email'=>'resercher@gmail.com',
				'password'=>'123456',
				'role'=>'RU',
            ],
            '2' => [
                'name' => 'Manager User',
                'first_name' => 'Manager',
                'last_name' => '',
				'status'=>'active',
				'email'=>'manager@gmail.com',
				'password'=>'123456',
				'role'=>'MU',
            ],
			'3' => [
                'name' => 'Lawyer User',
                'first_name' => 'Lawyer',
                'last_name' => '',
				'status'=>'active',
				'email'=>'lawyer@gmail.com',
				'password'=>'123456',
				'role'=>'LU',
            ],
        ],	

];
